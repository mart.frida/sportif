window.addEventListener("DOMContentLoaded", () => {

  //details  
  const details = document.querySelector(".details");
  const listLi = details.querySelectorAll("li");
  listLi.forEach((li) => {
    li.className = "font-21";
    li.style.fontSize = "1.25rem";
  });

  //data product
  const prContainer = document.querySelector(".product-container");
  const prImg = prContainer.querySelector(".img-min").firstChild;
  const prName = prContainer.querySelector("#pr_name").innerText;
  const prItem = prContainer.querySelector("#pr_item").innerText;
  const prPrice = prContainer.querySelector("#pr_price").innerText;
  console.log(prName, prItem, prPrice);

  //modal window - add to cart
  const openModalToCart = document.querySelector("#open_modal_to_cart");
  const dialog = document.querySelector("dialog");
  const continueShopping = dialog.querySelector("#continue_shopping");
  const btnOpen = dialog.querySelector("#open");
  const linkAdd=dialog.querySelector("#open");

  openModalToCart.onclick = () => dialog.showModal();
  continueShopping.onclick = () => dialog.close();

  btnOpen.addEventListener("click", () => {
    const cartStorage = localStorage.getItem("cart") || "[]";
    const cart = JSON.parse(cartStorage);
    const card = { prName, prItem, prPrice };
    localStorage.setItem("cart", JSON.stringify([...cart, card]));
    dialog.close();
  });

  //modal window - add to wishlist
  const openModalToWishlist = document.querySelector("#open_modal_to_wishlist");
  const dInnerText = dialog.querySelector("p");
  openModalToWishlist.onclick = () => {
    dialog.showModal();
    dInnerText.innerText = "The product has been added to the wishlist";
    btnOpen.innerText = "Open wishlist";
    linkAdd.href="#"
  };
  
  continueShopping.onclick = () => dialog.close();
});
