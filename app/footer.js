window.addEventListener("DOMContentLoaded", () => {
  const footer = document.createElement("footer");
  document.body.append(footer);

  function UlList(name) {
    this.name = name;
    this.cteateUlList = function () {
      const ul = document.createElement("ul");
      ul.innerText = this.name;
      ul.className = "font-16";
      footer.appendChild(ul);

      this.createElLi = function (inText, href) {
        const li = document.createElement("li");
        const a = document.createElement("a");
        ul.append(li);
        li.append(a);
        li.className = "font-142";
        li.style.cursor = "pointer";
        a.innerText = inText;
        a.setAttribute("href", href);
        // a.setAttribute("target", "_blank");
      };

      this.appendEl = function (elem) {
        ul.append(elem);
      };
    };
  }
  const ulOrderOnline = new UlList("ORDERING ONLINE");
  ulOrderOnline.cteateUlList();
  ulOrderOnline.createElLi("Account Login", "#");
  ulOrderOnline.createElLi("Our Guarantee", "#");
  ulOrderOnline.createElLi("Sportif Stretch Guide", "#");
  ulOrderOnline.createElLi("Size Chart & Sizing Information", "#");

  const ulAbout = new UlList("ABOUT SPORTIF");
  ulAbout.cteateUlList();
  ulAbout.createElLi("COVID-19 Response", "#");
  ulAbout.createElLi("History", "#");
  ulAbout.createElLi("Legacy", "#");

  const ulQuick = new UlList("QUICK LINKS");
  ulQuick.cteateUlList();
  ulQuick.createElLi("FAQs", "#");
  ulQuick.createElLi("Shop Online Catalog", "#");
  ulQuick.createElLi("Contact Us", "#");

  const ulToKnow = new UlList("GET TO KNOW US");
  ulToKnow.cteateUlList();
  
  ulToKnow.createElLi(
    "Sign up for our weekly newsletter and get a 10% off coupon by email for your first order!",
    "#"
  );
  ulToKnow.createElLi("Shop Online Catalog", "#");
  ulToKnow.createElLi("Contact Us", "#");

  //form email
  // const formEmail = document.createElement("form");
  // formEmail.innerText = "Email Address";
  // formEmail.setAttribute("action", "/action_page.html");
 
  // ulToKnow.appendEl(formEmail);
  // const inputSub = document.createElement("input");
  // inputSub.setAttribute("value", "Subscribe");
  // inputSub.setAttribute("type","submit");
  // formEmail.append(inputSub);
//form2

{/* <FORM>
Код: <INPUT NAME="cod"> <BR>
Телефон: <INPUT NAME="phone" SIZE="6"
MAXLENGTH="6"> <BR>
<P>
<INPUT TYPE = RESET > <INPUT TYPE=SUBMIT>
</ FORM> */}

  const formEmail = document.createElement("form");
   formEmail.setAttribute("action", "/action_page.html");
   ulToKnow.appendEl(formEmail);

  const inputSub = document.createElement("input");
  inputSub.setAttribute("value", "Enter your email");
  inputSub.setAttribute("name","subscribe");
  inputSub.setAttribute("type","text");
  formEmail.append(inputSub);

  const inputSubBtn = document.createElement("input");
  formEmail.append(inputSubBtn);
  inputSubBtn.className="btn-submit"
  inputSubBtn.setAttribute("type","submit");
  inputSubBtn.setAttribute("value", "Submit");

  //Up Arrow
  const upArrow = document.createElement("button");
  upArrow.setAttribute("id", "up_arrow");
  document.body.append(upArrow);
  upArrow.onclick = () => topFunction();

  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    upArrow.style.display = "flex";
  } else {
    upArrow.style.display = "none";
  }
});


// додати функціонал форми
