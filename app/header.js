//додати функціонал кнопці пошуку

window.addEventListener("DOMContentLoaded", () => {
  const nav = document.createElement("nav");
  document.body.prepend(nav);
  const logoLink = document.createElement("a");
  nav.prepend(logoLink);

  const logo = new Image();
  logo.src = "./media/logo.png";
  logoLink.append(logo);
  logoLink.setAttribute("href", "../asport/homepage.html");

  const ulNav = document.createElement("ul");
  nav.append(ulNav);
  ulNav.className = "ulNav";

  function createElNav(name, href) {
    const li = document.createElement("li");
    const a = document.createElement("a");
    li.className = "ulNavLi font-142";
    li.style.cursor = "pointer";
    ulNav.appendChild(li);
    li.append(a);
    a.innerText = name;
    a.setAttribute("href", href);
    // a.setAttribute("target", "_blank");
  }
  const elNav1 = createElNav("Shorts", "../asport/catalog-page.html");
  const elNav2 = createElNav("Pants", "#");
  const elNav3 = createElNav("Shirts", "#");
  const elNav4 = createElNav("Accessories", "#");
  const elNav5 = createElNav("Sale", "#");

  const elNav6 = document.createElement("li");
  elNav6.className = "ulNavLi font-142";
  const formSearch = document.createElement("form");
  const textSearch = document.createElement("input");
  const btnSearch = document.createElement("input");
  ulNav.append(elNav6);
  elNav6.append(formSearch);
  formSearch.append(btnSearch, textSearch);
  textSearch.setAttribute("type", "search");
  textSearch.setAttribute("name", "nameSearch");
  textSearch.setAttribute("autocomplete", "on");
  btnSearch.setAttribute("type", "submite");
  btnSearch.className = "searchBtn";

  const mediaQuery1024 = window.matchMedia("(min-width: 1024px)");
  const mediaQuery768 = window.matchMedia("(min-width: 768px)");

  if (mediaQuery1024.matches) {
    textSearch.setAttribute("placeholder", "Search entire store here");
  } else if (mediaQuery768.matches) {
    textSearch.setAttribute("placeholder", "Search");
  } else {
    textSearch.setAttribute("placeholder", "");
  }

  
});
