// const dialogCart = document.createElement("dialog");
// const pDialog = document.createElement("p");
// const btnClose = document.createElement("button");
// dialogCart.setAttribute("id","buy_modal");
// p.innerText="Thank you for your order. Your request is being processed."
// btnClose.setAttribute("id","close_modal");
// btnClose.innerText="Close";

// body.append(dialogCart);
// dialogCart.append(pDialog, btnClose);

//dialog product-page
const dialogProduct = document.createElement("dialog");
const pDialog = document.createElement("p");
const btnOpenCart = document.createElement("a");
const btnContinue = document.createElement("button");

dialogProduct.setAttribute("id", "add_modal");

pDialog.innerText = "The product has been added to the cart.";
btnOpenCart.setAttribute("id", "open");
btnOpenCart.setAttribute("href", "./cart-page.html");
btnOpenCart.setAttribute("type", "btn");
btnOpenCart.setAttribute("target", "_blank");
btnOpenCart.innerText = "Open cart";
btnContinue.setAttribute("id", "continue_shopping");
btnContinue.innerText = "Сontinue shopping";

body.append(dialogProduct);
dialogProduct.append(pDialog, btnOpenCart, btnContinue);
