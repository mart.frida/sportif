window.addEventListener("DOMContentLoaded", () => {
  //btnsShop
  const btnsShop = document.querySelectorAll(".btnShop");
  btnsShop.forEach((btnShop) => {
    btnShop.addEventListener("click", () => {
      btnShop.setAttribute("formaction", "./catalog-page.html");
    });
  });

  const listProducts = document.querySelector(".list-products");

  //product card
  class ProductItemCard {
    constructor(name, price, imgSrc) {
      this.imgSrc = imgSrc;
      this.name = name;
      this.price = price;
    }

    createCard() {
      const productCard = document.createElement("div");
      productCard.className = "product-card";
      listProducts.appendChild(productCard);

      const img = document.createElement("img");
      img.setAttribute("src", this.imgSrc);

      const name = document.createElement("p");
      name.innerText = this.name;
      name.className = "product-name font-15";

      const stars = document.createElement("img");
      stars.setAttribute("src", "../asport/media/Stars.png");
      stars.setAttribute("alt", "stars");

      const priceline = document.createElement("p");
      priceline.className = "product-price font-12";
      priceline.innerText = "As low as ";
      const price = document.createElement("b");
      price.innerText = `$${this.price}`;
      priceline.appendChild(price);

      const btnToCart = document.createElement("button");
      btnToCart.className = "product-add";
      btnToCart.innerText = "ADD TO CART";
      productCard.append(img, name, stars, priceline, btnToCart);

      btnToCart.onclick = () => dialog.showModal();
    }
  }

  const p1 = new ProductItemCard(
    "Marchal's Original Cargo Short",
    "40.99",
    "../asport/media/image 3.png"
  );
  p1.createCard();

  const p2 = new ProductItemCard(
    "Inlet Original Short",
    "38.99",
    "../asport/media/image 4.png"
  );
  p2.createCard();

  const p3 = new ProductItemCard(
    "TGIF Pant",
    "40.99",
    "../asport/media/image 5.png"
  );
  p3.createCard();

  const p4 = new ProductItemCard(
    "Inlet Original Pant",
    "52.99",
    "../asport/media/image 6.png"
  );
  p4.createCard();

  //modall window
  const dialog = document.querySelector("dialog");
  const btnOpen = dialog.querySelector("#open");
  const btnContinue = dialog.querySelector("#continue_shopping");
  btnContinue.onclick = () => dialog.close();
  //data product
  const prContainer = document.querySelectorAll(".product-card");
  prContainer.forEach((el) => {
    const prImg = el.querySelector("img");
    const prName = el.querySelector(".product-name").innerText;
    const prItem = "1235";
    const prPrice = el.querySelector("b").innerText;
    btnOpen.addEventListener("click", () => {
      const cartStorage = localStorage.getItem("cart") || "[]";
      const cart = JSON.parse(cartStorage);
      const card = { prName, prItem, prPrice };
      localStorage.setItem("cart", JSON.stringify([...cart, card]));
      dialog.close();
    });
  });
});
