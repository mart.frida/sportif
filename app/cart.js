const cartContainer = document.querySelector("#cart_container");

const cartStorage = JSON.parse(localStorage.getItem("cart") || "[]");

const btnBuy = document.querySelector("#btn_buy");
const dialogBuy = document.querySelector("#buy_modal");
const closeDialog = dialogBuy.querySelector("#close_modal");

if (cartStorage.length) {
  cartStorage.forEach((el) => {
    const { prName, prItem, prPrice } = el;
    const productItem = document.createElement("div");
    productItem.className = "product_item";
    // newCard.style.height="200px"
    productItem.innerHTML = `<div class="content"><div class="title font-142">${prName} - ${prItem}</div>
    <div class="product_qty_price"><div class="price font-15">${prPrice}</div></div></div></div>`;
    cartContainer.prepend(productItem);
  });
}

btnBuy.onclick = () => dialogBuy.showModal();
closeDialog.onclick = () => dialogBuy.close();
